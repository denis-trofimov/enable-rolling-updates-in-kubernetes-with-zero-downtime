# Enable Rolling updates in Kubernetes with Zero downtime

## Repeating results of Nilesh Jayanandana

<https://medium.com/platformer-blog/enable-rolling-updates-in-kubernetes-with-zero-downtime-31d7ec388c81>

## Deploy

```sh
$ kubectl apply -f deployment.yaml 
deployment.apps/hello-dep created
$ kubectl get po
NAME                         READY   STATUS    RESTARTS   AGE
hello-dep-6767885458-7t2t2   1/1     Running   0          13s
hello-dep-6767885458-fvbm8   1/1     Running   0          13s

$ kubectl expose deployment hello-dep --type LoadBalancer   --port 80 --target-port 8080
service/hello-dep exposed
$ curl 127.0.0.1:80
Hello, world!
Version: 1.0.0
Hostname: hello-dep-6767885458-fvbm8
```

### Redeploy and test in the same time

```sh
$ kubectl apply -f deployment.yaml && kubectl get po
deployment.apps/hello-dep unchanged
NAME                         READY   STATUS    RESTARTS   AGE
hello-dep-5fcb7795f8-nnc87   1/1     Running   0          8m12s

```

## Test in the same time in another console, run ApacheBench

```sh
$ ab -l -n 5000 http://127.0.0.1:80/ 
This is ApacheBench, Version 2.3 <$Revision: 1826891 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)
Completed 500 requests
Completed 1000 requests
Completed 1500 requests
Completed 2000 requests
Completed 2500 requests
Completed 3000 requests
Completed 3500 requests
Completed 4000 requests
Completed 4500 requests
Completed 5000 requests
Finished 5000 requests


Server Software:        
Server Hostname:        127.0.0.1
Server Port:            80

Document Path:          /
Document Length:        Variable

Concurrency Level:      1
Time taken for tests:   7.205 seconds
Complete requests:      5000
Failed requests:        0
Total transferred:      915000 bytes
HTML transferred:       330000 bytes
Requests per second:    693.97 [#/sec] (mean)
Time per request:       1.441 [ms] (mean)
Time per request:       1.441 [ms] (mean, across all concurrent requests)
Transfer rate:          124.02 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       2
Processing:     1    1   0.5      1      12
Waiting:        1    1   0.5      1      10
Total:          1    1   0.5      1      12

Percentage of the requests served within a certain time (ms)
  50%      1
  66%      1
  75%      1
  80%      2
  90%      2
  95%      2
  98%      3
  99%      3
 100%     12 (longest request)

```

## Clean up

```sh
$ kubectl delete service hello-dep
service "hello-dep" deleted
$ kubectl delete deployment hello-dep
deployment.extensions "hello-dep" deleted
```